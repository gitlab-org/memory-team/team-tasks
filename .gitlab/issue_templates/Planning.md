*This page may contain information related to upcoming products, features and functionality.
It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes.
Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features, or functionality remain at the sole discretion of GitLab Inc.*

# Capacity

List any noteworthy PTO that may impact the capacity for the planned milestone

# Planning

Indicate major efforts by linking to the appropriate epic. Under each epic indicate who is focusing on these efforts and list the individual issues assigned for this milestone. 

## Top Priorities for `milestone`

### [Epic Title]()

Who: 

Issues
- Issue link 1
- Issue link 2 


## Additional Issues for consideration

### Tech Debt / Tidy ups

https://gitlab.com/groups/gitlab-org/-/epics/15902+ let's pick a couple from here to do.

# Kickoff Video

/label ~"group::cloud connector" ~"devops::data_stores" ~"section::enablement"
/assign @pjphillips @sguyon
